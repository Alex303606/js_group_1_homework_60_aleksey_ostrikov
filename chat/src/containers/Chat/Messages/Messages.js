import React from 'react';
import Message from "./Message/Message";

const Messages = props => {
	return (
		<ul id="list">
			{
				props.messages.map(message => {
					return <Message
						key={message._id}
						author={message.author}
						message={message.message}
						datetime={message.datetime}
					/>
				})
			}
		</ul>
	)
};

export default Messages;