import React from 'react';

const SendBlock = props => {
	return (
		<div className="send-block">
			<form action="#">
				<input type="text" onChange={props.changeAuthor} value={props.author}/>
				<textarea onChange={props.makeTextMessage} value={props.text} placeholder="Your message:"></textarea>
				<button onClick={props.sendMessage} className="send">Send</button>
			</form>
		</div>
	)
};

export default SendBlock;